const gulp = require('gulp')
const $connect = require('gulp-connect-php')
const $sass = require('gulp-sass')
const $changed = require('gulp-changed')
const $sourcemaps = require('gulp-sourcemaps')
const $plumber = require('gulp-plumber')
const $postcss = require('gulp-postcss')
const $notify = require('gulp-notify')

const del = require('del')
const autoprefixer = require('autoprefixer')
const mqpacker = require('css-mqpacker')
const browserSync = require('browser-sync')

const config = require('./gulp.config')

const browserReloadOptions = { once: true }
const streamBrowser = browserSync.stream.bind(null, browserReloadOptions)

const handleErrorOnPlumber = $notify.onError({
  message: 'Error <%= error.message %>',
  ...config.notificationOptions,
})

gulp.task('sass', () => {
  const postcssProcessors = [
    autoprefixer(config.autoprefixerOptions),
    mqpacker,
  ]
  return gulp.src(config.src.scss)
    .pipe($plumber(handleErrorOnPlumber))
    .pipe($changed(config.app.css))
    .pipe($sourcemaps.init())
    .pipe($sass(config.sassOptions))
    .pipe($postcss(postcssProcessors))
    .pipe($sourcemaps.write('./'))
    .pipe(gulp.dest(config.app.css))
    .pipe(streamBrowser())
})

gulp.task('serve', (done) => {
  $connect.server(config.connectOptions, () => {
    browserSync.init(config.browserSyncOptions)
  })
  done()
})

gulp.task('clean', (done) => {
  const options = { dot: true }
  del([config.app.css], options)
  done()
})

gulp.task('watch', (done) => {
  gulp.watch(config.app.php).on('change', browserSync.reload)
  gulp.watch(config.app.img).on('change', browserSync.reload)
  gulp.watch(config.app.js).on('change', browserSync.reload)
  gulp.watch(config.src.scss, gulp.series('sass'))
  done()
})

gulp.task('build', gulp.series(...[
  'clean',
  'sass',
]))

gulp.task('default', gulp.series(...[
  'clean',
  gulp.parallel('serve', 'watch', 'sass'),
]))
