module.exports = {
  root: true,
  env: {
    jquery: true,
    browser: true,
  },
  extends: 'standard',
  // add your custom rules here
  rules: {
    'arrow-parens': ['error', 'always'],
    'comma-dangle': ['error', 'always-multiline'],
    'curly': 'error',
    'default-case': 'error',
    'indent': ['error', 2, { 'SwitchCase': 1 }],
    'no-console': 'warn',
    'no-implicit-coercion': 'error',
    'no-mixed-operators': 'error',
    'no-var': 'error',
    'prefer-arrow-callback': 'error',
    'no-unused-vars': ['warn', { vars: 'all' }],
  },
}
