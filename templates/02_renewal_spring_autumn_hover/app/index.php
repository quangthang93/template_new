<?php

// 開発用
include_once $_SERVER['DOCUMENT_ROOT'] . '/app/assets/inc/config.php';

// 本番用
// include_once $_SERVER['DOCUMENT_ROOT'].'/page/autumn_newshop/assets/inc/config.php';
?>

<!DOCTYPE html>
<html lang="ja">

  <head>
    <?php include_once $_SERVER['DOCUMENT_ROOT'].'/'.DIRNAME.'/assets/inc/_shop/'.STORE_NAME.'/tagmanager1.php';?>
    <?php include_once $_SERVER['DOCUMENT_ROOT'].'/'.DIRNAME.'/assets/inc/html-head.php'; ?>
  </head>

  <body class="page-tag">
    <?php include_once $_SERVER['DOCUMENT_ROOT'].'/'.DIRNAME.'/assets/inc/_shop/'.STORE_NAME.'/tagmanager2.php';?>
    <?php include_once $_SERVER['DOCUMENT_ROOT'].'/'.DIRNAME.'/assets/inc/blocks/svgs.php';?>

    <a name="top" id="top"></a>

    <?php include_once $_SERVER['DOCUMENT_ROOT'].'/'.DIRNAME.'/assets/inc/blocks/header.php'; ?>

    <main class="contents">
      <div class="contents__inner">

      <?php  include_once $_SERVER['DOCUMENT_ROOT'].'/'.DIRNAME.'/assets/inc/blocks/tab.php'; ?>

        <div class="contents__shop-list">

        <?php  include_once $_SERVER['DOCUMENT_ROOT'].'/'.DIRNAME.'/assets/inc/blocks/shop-list.php'; ?>

        </div>

        <?php // include_once $_SERVER['DOCUMENT_ROOT'].'/'.DIRNAME.'/assets/inc/blocks/foot-banner.php'; ?>

      </div>

      <?php include_once $_SERVER['DOCUMENT_ROOT'].'/'.DIRNAME.'/assets/inc/blocks/footer.php'; ?>

    </main>

    <?php include_once $_SERVER['DOCUMENT_ROOT'].'/'.DIRNAME.'/assets/inc/scripts.php'; ?>

  </body>

</html>
