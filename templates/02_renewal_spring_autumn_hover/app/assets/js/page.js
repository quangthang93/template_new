/* !item information -------------------------------------------------------------- */
$(() => {
  const $buttons = $('.js-shop-button-more')
  $buttons.on('click', function () {
    $(this).toggleClass('is-open')
    $(this).prev().slideToggle()
    return false
  })
})

/* !category nav -------------------------------------------------------------- */
$(() => {
  const $tabButtons = $('.js-classification-tab')
  const $shops = $('.js-shop')

  $tabButtons.on('click', function () {
    $tabButtons.removeClass('is-current')
    $(this).addClass('is-current')

    const classification = $(this).data('select')
    let $targets

    if (classification === 'all') {
      $targets = $shops
    } else {
      $targets = $shops.filter(`[data-classification=${classification}]`)
    }

    $.when(
      $shops.fadeOut()
    ).then(() => {
      $targets.fadeIn()
    })

    return false
  })
})
