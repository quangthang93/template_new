/* !smooth-scroll -------------------------------------------------------------- */
$(() => {
  $('a.scroll').on('click', function () {
    const options = {
      duration: 400,
      easing: 'swing',
    }
    const href = $(this).attr('href')
    const target = $(href === '#' || href === '' ? 'html' : href)

    $('body, html').animate({
      scrollTop: target.offset().top,
    }, options)

    return false
  })
})
