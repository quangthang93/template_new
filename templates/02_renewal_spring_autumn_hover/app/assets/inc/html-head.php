<?php /* ==========================================================
title、description、OGPなどはページごとに違う値を設定します。
また値については変数や定数で設定します。
config.phpや各index.php、indexXXXX.phtmlのファイル先頭付近の設定をご確認ください。
============================================================== */ ?>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $pege_title; ?></title>
<meta name="description" content="<?php echo $pege_description; ?>">
<meta name="keywords" content="<?php echo $pege_keywords; ?>">

<!-- OGP -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/'.DIRNAME.'/assets/inc/ogp.php';?>
<!-- /OGP -->

<link rel="stylesheet" href="/<?php echo DIRNAME; ?>/assets/css/style.css" />
