<?php /* ==========================================================
SNS公式アカウントリスト
============================================================== */ ?>
  <ul class="phd__sns"><!--
  --><li class="phd__sns__item"><a href="https://www.facebook.com/urawaparco/" class="phd__sns__item__link-facebook" target="_blank">
          <svg class="facebook">
              <title>Facebook</title>
              <desc>Facebook</desc>
              <use xlink:href="#icon-facebook"/>
          </svg>
      </a></li><!--
  --><li class="phd__sns__item"><a href="https://twitter.com/PARCO_Urawa" class="phd__sns__item__link-twitter" target="_blank">
          <svg class="twitter">
              <title>Twitter</title>
              <desc>Twitter</desc>
              <use xlink:href="#icon-twitter"/>
          </svg>
      </a></li><!--
  --><li class="phd__sns__item"><a href="https://page.line.me/urawaparco" class="phd__sns__item__link-lineat" target="_blank">
          <svg class="line">
              <title>LINE@</title>
              <desc>LINE@</desc>
              <use xlink:href="#icon-lineat"/>
          </svg>
      </a></li><!--
<?php /* ?>
  --><li class="phd__sns__item"><a href="https://www.instagram.com/parco_kumamoto_official/" class="phd__sns__item__link-instagram" target="_blank">
          <svg class="instagram">
              <title>Instagram</title>
              <desc>Instagram</desc>
              <use xlink:href="#icon-instagram"/>
          </svg>
      </a></li><!--
<?php */ ?>
  --></ul>
