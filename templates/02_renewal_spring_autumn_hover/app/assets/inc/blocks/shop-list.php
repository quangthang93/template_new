<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/app/assets/inc/config.php';

$filepath = $_SERVER['DOCUMENT_ROOT'] . '/' . DIRNAME . '/assets/csv/shop-list.csv';
$file = new SplFileObject($filepath);

$file->setFlags(
  SplFileObject::READ_CSV |
  SplFileObject::SKIP_EMPTY |
  SplFileObject::READ_AHEAD
);

$shop_list = array();
$keys = str_getcsv($file->fgets());

foreach ($file as $i => $row) {
  if ($i === 0) continue;

  $record = array();
  foreach ($row as $j => $col) {
    $key = $keys[$j];
    $record[$key] = $col;
  }
  $shop_list[] = $record;
}

function generate_item_number ($index) {
  return ($index + 1) < 10 ? '0' . ($index + 1) : $index + 1;
}

function sanitize ($str) {
  return nl2br(htmlspecialchars($str, ENT_NOQUOTES, 'UTF-8'));
}

?>


<div class="shop-list">

  <?php foreach ($shop_list as $i => $shop): ?>

  <?php
  $img_index = generate_item_number($i);
  $img_path = array(
    'sp' => DIRNAME . '/assets/images/shop-list/sp_img_' . $img_index . '.jpg',
    'pc' => DIRNAME . '/assets/images/shop-list/pc_img_' . $img_index . '.jpg',
  );
  $logo_path = array(
    'sp' => DIRNAME . '/assets/images/shop-list/sp_logo_' . $img_index . '.png',
    'pc' => DIRNAME . '/assets/images/shop-list/pc_logo_' . $img_index . '.png',
  );

  $classification = $shop['new_renewal'];
  $classification_lower = mb_strtolower($classification);
  ?>

  <section class="shop-list__item js-shop" data-classification="<?= sanitize($classification_lower) ?>">

    <?php
    // ----- SP -----
    ?>
    <div class="shop">
      <img class="shop__photo" src="<?= $img_path['sp'] ?>" alt="<?= sanitize($shop['name']) ?>">

      <a href="#" class="shop__link">
        <div class="shop__header">
          <h1 class="shop__name"><?= sanitize($shop['name']) ?></h1>
          <p class="shop__floor"><?= sanitize($shop['floor']) ?></p>
        </div>

        <div class="shop__body">
          <div class="shop__info">
            <div class="shop__date"><?= sanitize($shop['date']) ?><br><?= sanitize($shop['date_week']) ?></div>
            <div class="shop__classification"><?= sanitize($classification) ?></div>
          </div>
          <div class="shop__logo">
            <?php if ($i === 6): ?>
            <?php // SP ロゴ無し ?>
            <?php else: ?>
            <img class="shop__logo-image" src="<?= $logo_path['sp'] ?>" alt="<?= sanitize($shop['name']) ?>">
            <?php endif; ?>
          </div>
        </div>
      </a>

      <div class="shop__footer">
        <p class="shop__description"><?= sanitize($shop['description']) ?></p>
        <button class="shop__button-more js-shop-button-more"></button>
      </div>
    </div>

    <?php
    // ----- PC -----
    ?>

    <div class="shop-pc">
      <a href="#" class="shop-pc__link">
        <img class="shop-pc__photo" src="<?= $img_path['pc'] ?>" alt="<?= sanitize($shop['name']) ?>">

        <div class="shop-pc__body">
          <p class="shop-pc__info-wrap">
            <span class="shop-pc__info"><?= sanitize($shop['date']) ?> <?= sanitize($shop['date_week']) ?> | <?= sanitize($shop['new_renewal']) ?></span>
          </p>

          <p class="shop-pc__floor"><?= sanitize($shop['floor']) ?></p>
          <h1 class="shop-pc__name"><?= sanitize($shop['name']) ?></h1>
          <p class="shop-pc__logo">
            <img class="shop-pc__logo-image" src="<?= $logo_path['pc'] ?>" alt="<?= sanitize($shop['name']) ?>">
          </p>
          <p class="shop-pc__description"><?= sanitize($shop['description']) ?></p>
        </div>
      </a>
    </div>
  </section>

  <?php endforeach; ?>

</div>
