<?php /* ===========================================================
共通ヘッダー
============================================================== */ ?>
<div class="phd">
  <div class="phd__in">

    <p class="phd__logo">
      <a href="/">
        <svg class="phd__logo__txt">
          <title><?php echo STORE_NAME_JA; ?></title>
          <desc><?php echo STORE_NAME_JA; ?></desc>
          <use xlink:href="#parco-logo-<?php echo STORE_NAME; ?>"/>
        </svg>
        <svg class="phd__logo__img">
          <title>PARCO</title>
          <desc>PARCO</desc>
          <use xlink:href="#parco-logo"/>
        </svg>
      </a>
    </p>

    <?php include_once $_SERVER['DOCUMENT_ROOT'].'/'.DIRNAME.'/assets/inc/_shop/'.STORE_NAME.'/page-hd-snslist.php'; ?>

  </div>
</div>

<div class="header">

  <h1 class="header__title">
    <img src="/<?php echo DIRNAME; ?>/assets/images/title_pc.png" alt="<?php echo $pege_title; ?>" class="title_pc" />
    <img src="/<?php echo DIRNAME; ?>/assets/images/title_sp.png" alt="<?php echo $pege_title; ?>" class="title_sp" />
  </h1>

</div>
