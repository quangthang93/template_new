<ul class="tab-list">
  <li class="tab-list__item">
    <a class="tab-list__link js-classification-tab is-current" data-select="all"><span class="tab-list__txt-en">ALL</span><br><span class="tab-list__txt-ja">全ての店舗</span></a>
  </li>
  <li class="tab-list__item">
    <a class="tab-list__link js-classification-tab" data-select="new"><span class="tab-list__txt-en">NEW</span><br><span class="tab-list__txt-ja">新店舗</span></a>
  </li>
  <li class="tab-list__item">
    <a class="tab-list__link js-classification-tab" data-select="renewal"><span class="tab-list__txt-en">RENEWAL</span><br><span class="tab-list__txt-ja">新装開店</span></a>
  </li>
</ul>
