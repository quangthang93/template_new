<div class="pft">
  <div class="pft__in">

    <div class="pft__logo">
      <a href="/">
        <span class="pft__logo__txt"><?php echo STORE_NAME_JA; ?></span>
        <svg class="pft__logo__img">
          <title>PARCO</title>
          <desc>PARCO</desc>
          <use xlink:href="#parco-logo"/>
        </svg>
      </a>
    </div>

    <div class="pft__pageTop">
      <a href="#top" class="scroll">ページトップ</a>
    </div>

    <div class="pft__copyright">
      <svg class="pft__copyright__img">
        <title>COPYRIGHT &copy; PARCO CO.,LTD ALL RIGHTS RESERVED.</title>
        <desc>COPYRIGHT &copy; PARCO CO.,LTD ALL RIGHTS RESERVED.</desc>
        <use xlink:href="#copyright"/>
      </svg>
    </div>

  </div>
</div>
