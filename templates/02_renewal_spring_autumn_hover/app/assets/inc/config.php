<?php
define('STORE_NAME', 'tsudanuma'); //店名 英語
define('STORE_NAME_JA', '津田沼'); //店名 日本語
define('DIRNAME', 'app'); //assetsを置くディレクトリ 例はhttps://xxxx.parco.jp/app/ に設置する場合
// define('DIRNAME', 'page/demo'); page/demo/ に設置する場合

//ページ用の変数
$pege_title = 'NEW&RENEWAL | 静岡パルコ';
$pege_description = '静岡PARCOの秋のNEW&RENEWAL。新規オープンやリニューアルするショップの情報をご紹介いたします。';
$pege_keywords = '静岡パルコ、静岡、改装、リニューアル';
$page_shareurl = 'https://'.STORE_NAME.'.parco.jp/page/autumn_newshop/'; //必ずディレクトリ名を変更する
?>
