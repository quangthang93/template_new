<meta property="fb:admins" content="100005423256030">
<meta property="og:locale" content="ja_JP" />
<meta property="og:type" content="article" />
<meta property="og:title" content="<?php echo $pege_title; ?>" />
<meta property="og:url" content="<?php echo $page_shareurl; ?>" />
<meta property="og:site_name" content="<?php echo STORE_NAME_JA; ?>PARCO" />
<meta property="og:description" content="<?php echo $pege_description; ?>" />
<meta property="og:image" content="https://<?php echo STORE_NAME; ?>.parco.jp/<?php echo DIRNAME; ?>/assets/images/ogp.png" />
