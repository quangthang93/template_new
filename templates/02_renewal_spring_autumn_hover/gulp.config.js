const ACTUAL_PORT = 12000
const SYNCED_PORT = 3100
const SRC_DIR = 'src'
const APP_DIR = 'app'

module.exports = {
  port: SYNCED_PORT,

  src: {
    root: SRC_DIR,
    scss: `${SRC_DIR}/scss/**/*.scss`,
  },

  app: {
    root: APP_DIR,
    php: `${APP_DIR}/**/*.php`,
    css: `${APP_DIR}/assets/css`,
    img: `${APP_DIR}/assets/images`,
    js: `${APP_DIR}/assets/js/**/*.js`,
  },

  notificationOptions: {
    sound: false,
  },

  connectOptions: {
    port: ACTUAL_PORT,
    base: '.',
    stdio: 'ignore',
  },

  browserSyncOptions: {
    ui: false,
    open: false,
    notify: true,
    proxy: `localhost:${ACTUAL_PORT}/${APP_DIR}`,
    port: SYNCED_PORT,
  },

  autoprefixerOptions: {
    grid: true,
  },

  sassOptions: {
    outputStyle: 'expanded',
  },
}
