module.exports = {
  extends: [
    'stylelint-scss',
  ],

  ignoreFiles: [
    './node_modules',
    './dist',
    '*.css',
    './src/**/scss/**/lib/**/*.scss',
  ],

  rules: {
    'indentation': 2,
    'no-descending-specificity': null,
    'block-no-empty': null,
    'no-empty-source': null,
  },
}
