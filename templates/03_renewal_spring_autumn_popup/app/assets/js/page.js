(() => {
  let windowSize = $(window).width()
  let breakpoint = 769

  $(window).on('resize', () => {
    windowSize = $(window).width()
  })

  /* !item information -------------------------------------------------------------- */
  $(() => {
    let $buttons = $('.js-toggle-content')

    const toggle = ($el) => {
      $el
        .toggleClass('is-open')
        .prev()
        .slideToggle()
    }

    const close = ($el) => {
      $el
        .removeClass('is-open')
        .prev()
        .slideUp()
    }

    $buttons.on('click', function () {
      toggle($(this))
      return false
    })

    $(window).on('resize', () => {
      if (windowSize <= breakpoint) {
        return
      }
      if ($buttons.hasClass('is-open')) {
        close($buttons)
      }
    })
  })

  /* !modal
  ------------------------------------------------------------------------------------------------------------------------ */
  $(() => {
    let isWindowSizePc = false

    // loadする時にcolorbox生成
    $(window).on('load', () => {
      if (windowSize > breakpoint) {
        isWindowSizePc = true
      }

      $('.js-shop-modal-trigger').colorbox({
        inline: true,
        opacity: 0.8,
        returnFocus: false,
        width: '100%',
        maxWidth: 890,
        initialWidth: 200,
        reposition: false,
        fixed: true,
      })
    })

    // resizeして、SP画面表示であれば、モーダルを閉じる
    $(window).on('resize', () => {
      if (windowSize <= breakpoint) {
        if (isWindowSizePc) {
          $.colorbox.close()
        }
        isWindowSizePc = false
        return
      }

      isWindowSizePc = true
    })
  })
})()
