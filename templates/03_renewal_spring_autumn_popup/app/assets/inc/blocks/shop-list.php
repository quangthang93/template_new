<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/app/assets/inc/config.php';

$filepath = $_SERVER['DOCUMENT_ROOT'] . '/' . DIRNAME . '/assets/csv/shop-list.csv';
$file = new SplFileObject($filepath);

$file->setFlags(
  SplFileObject::READ_CSV |
  SplFileObject::SKIP_EMPTY |
  SplFileObject::READ_AHEAD
);

$shop_list = array();
$keys = str_getcsv($file->fgets());

foreach ($file as $i => $row) {
  if ($i === 0) continue;

  $record = array();
  foreach ($row as $j => $col) {
    $key = $keys[$j];
    $record[$key] = $col;
  }
  $shop_list[] = $record;
}

function generate_item_number ($index) {
  return ($index + 1) < 10 ? '0' . ($index + 1) : $index + 1;
}

function sanitize ($str) {
  return nl2br(htmlspecialchars($str, ENT_NOQUOTES, 'UTF-8'));
}

?>

<div class="shop-list">
  <div class="shop-list__items">

    <?php foreach ($shop_list as $i => $shop): ?>

    <?php
    $ref_id = 'renew-' . generate_item_number($i);
    $img_index = generate_item_number($i);
    $img_path = array(
      'sp' => DIRNAME . '/assets/images/shop-list/sp_img_' . $img_index . '.jpg',
      'pc' => DIRNAME . '/assets/images/shop-list/pc_img_' . $img_index . '.jpg',
    );
    $logo_path = array(
      'sp' => DIRNAME . '/assets/images/shop-list/sp_logo_' . $img_index . '.png',
      'pc' => DIRNAME . '/assets/images/shop-list/pc_logo_' . $img_index . '.png',
    );

    $classification = $shop['new_renewal'];
    $classification_lower = mb_strtolower($classification);
    ?>

    <section class="shop-list__item">

      <div class="shop">
        <a href="#<?= $ref_id ?>" class="shop__modal-trigger js-shop-modal-trigger"></a>

        <img class="shop__photo" src="<?= $img_path['sp'] ?>" alt="<?= sanitize($shop['name']) ?>">

        <div class="shop__center-block">
          <a href="#" class="shop__link">
            <div class="shop__info">
              <div class="shop__date-day"><?= sanitize($shop['date']) ?></div>
              <div class="shop__date-week"><?= sanitize($shop['date_week']) ?></div>
              <div class="shop__classification"><?= sanitize($classification) ?></div>
            </div>

            <div class="shop__logo"><img class="shop__logo-image" src="<?= $logo_path['sp'] ?>" alt="<?= sanitize($shop['name']) ?>"></div>
          </a>
        </div>

        <div class="shop__bottom-block">
          <h1 class="shop__name"><?= sanitize($shop['name']) ?></h1>

          <p class="shop__floor"><?= sanitize($shop['floor']) ?>/<?= sanitize($shop['category']) ?></p>

          <div class="shop__description">
            <p class="shop__description-text"><?= sanitize($shop['description']) ?></p>

            <?php if (!empty($shop['special_heading'])): ?>
            <p class="shop__special-heading">★<?= sanitize($shop['special_heading']) ?></p>
            <p class="shop__special-text"><?= sanitize($shop['special_text']) ?></p>
            <?php endif; ?>

          </div>

          <button class="shop__toggle-button js-toggle-content"></button>
        </div>
      </div>

      <?php
      // ----- Modal -----
      ?>
      <div class="shop-modal">
        <div class="shop-modal__content" id="<?= $ref_id ?>">
          <div class="shop-modal__photo"><img class="shop-modal__photo-image" src="<?= $img_path['pc'] ?>" alt="<?= $shop['name'] ?>"></div>

          <div class="shop-modal__detail">
            <p class="shop-modal__info"><?= sanitize($shop['date']) ?> <?= sanitize($shop['date_week']) ?> <?= sanitize($classification) ?></p>
            <h2 class="shop-modal__name"><a class="shop-modal__name-link" href="https://kichijoji.parco.jp/shop/detail/?cd=024486"><?= sanitize($shop['name']) ?><i class="shop-modal__name-arrow"></i></a></h2>
            <p class="shop-modal__floor-category"><?= sanitize($shop['floor']) ?>/<?= sanitize($shop['category']) ?></p>
            <p class="shop-modal__paragraph"><?= sanitize($shop['description']) ?></p>

            <?php if (!empty($shop['special_heading'])): ?>
            <h3 class="shop-modal__special-heading"><?= sanitize($shop['special_heading']) ?></h3>
            <p class="shop-modal__paragraph"><?= sanitize($shop['special_text']) ?></p>
            <?php endif; ?>

          </div>
        </div>
      </div>

    </section>

    <?php endforeach; ?>

  </div>

  <div class="shop-list__footer">
    <div class="shop-list__notes">
      <p class="shop-list__notes-item">※掲載内容は予告なく変更になる場合がございます。</p>
      <p class="shop-list__notes-item">※ノベルティはなくなり次第終了とさせていただきます。</p>
    </div>

    <div class="shop-list__coming-image">
      <img class="" src="<?= DIRNAME ?>/assets/images/sp_txt_coming.png" alt="comingsoon" width="161">
    </div>

    <div class="shop-list__coming-text">10月以降もNEW SHOP続々オープン！</div>
  </div>

</div>
