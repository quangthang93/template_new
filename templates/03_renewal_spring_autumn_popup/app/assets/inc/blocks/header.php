<div class="phd">
  <div class="phd__in">

    <p class="phd__logo">
      <a href="/">
        <svg class="phd__logo__txt">
          <title><?= STORE_NAME_JA ?></title>
          <desc><?= STORE_NAME_JA ?></desc>
          <use xlink:href="#parco-logo-kichijoji" />
        </svg>
        <svg class="phd__logo__img">
          <title>PARCO</title>
          <desc>PARCO</desc>
          <use xlink:href="#parco-logo" />
        </svg>
      </a>
    </p>

    <ul class="phd__sns">
      <li class="phd__sns__item">
        <a href="https://www.facebook.com/kichijojiparco/" class="phd__sns__item__link-facebook" target="_blank">
          <svg class="facebook">
            <title>Facebook</title>
            <desc>Facebook</desc>
            <use xlink:href="#icon-facebook" />
          </svg>
        </a>
      </li>

      <li class="phd__sns__item">
        <a href="https://twitter.com/parco_ikebukuro" class="phd__sns__item__link-twitter" target="_blank">
          <svg class="twitter">
            <title>Twitter</title>
            <desc>Twitter</desc>
            <use xlink:href="#icon-twitter" />
          </svg>
        </a>
      </li>

      <li class="phd__sns__item">
        <a href="https://page.line.me/kichijojiparco" class="phd__sns__item__link-lineat" target="_blank">
          <svg class="line">
            <title>LINE@</title>
            <desc>LINE@</desc>
            <use xlink:href="#icon-lineat" />
          </svg>
        </a>
      </li>

      <li class="phd__sns__item">
        <a href="https://www.instagram.com/parco_ikebukuro_official/" class="phd__sns__item__link-instagram" target="_blank">
          <svg class="instagram">
            <title>Instagram</title>
            <desc>Instagram</desc>
            <use xlink:href="#icon-instagram" />
          </svg>
        </a>
      </li>
    </ul>

  </div>
</div>

<div class="header">
  <h1 class="header__title sp-visible"><img src="<?= DIRNAME ?>/assets/images/sp_title.png" width="100%" alt="<?php echo $title; ?>" /></h1>
  <h1 class="header__title pc-visible"><img src="<?= DIRNAME ?>/assets/images/title.png" alt="<?php echo $title; ?>" /></h1>
</div>
