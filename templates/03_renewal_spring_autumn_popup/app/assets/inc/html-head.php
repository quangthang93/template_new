<?php /* ==========================================================
title、description、OGPなどはページごとに違う値を設定します。
また値については変数や定数で設定します。
config.phpや各index.php、indexXXXX.phtmlのファイル先頭付近の設定をご確認ください。
============================================================== */ ?>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<title><?php echo $title; ?></title>
<meta name="description" content="<?= $description ?>" />
<meta name="keywords" content="<?= $keywords ?>"/>
<link rel="stylesheet" href="<?= DIRNAME ?>/assets/css/style.css">

<!-- OGP -->
<meta property="fb:admins" content="100005423256030">
<meta property="og:locale" content="ja_JP" />
<meta property="og:type" content="article" />
<meta property="og:title" content="<?php echo $title; ?>" />
<meta property="og:url" content="<?php echo $shareurl; ?>" />
<meta property="og:site_name" content="<?=STORE_NAME_JA?>PARCO" />
<meta property="og:description" content="<?php echo $description; ?>" />
<meta property="og:image" content="http://<?=STORE_NAME?>.parco.jp/page/<?=DIRNAME?>/assets/img/ogp.png" />
<!-- /OGP -->
