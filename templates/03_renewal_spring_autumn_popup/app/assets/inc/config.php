<?php
define('STORE_NAME', 'kichijoji');
define('STORE_NAME_JA', '吉祥寺');
define('DIRNAME', 'app'); // assets を置くディレクトリ
define('APPBNRID', '12111'); //アプリバナー記事ID

//ページ用の変数
$title = 'AUTUMN NEW&RENEWAL | 改装店鋪一覧 |'. STORE_NAME_JA .'PARCO';
$description = STORE_NAME_JA .'パルコのAUTUMN NEW&RENEWAL 。2018年秋の新規オープン、リニューアル情報をご紹介しております。';
$keywords = '';
$shareurl = 'https://'. STORE_NAME.'.parco.jp/';
?>
