<?php
// 開発用
include_once $_SERVER['DOCUMENT_ROOT'] . '/app/assets/inc/config.php';

// 本番用
// include_once $_SERVER['DOCUMENT_ROOT'].'/page/autumn_newshop/assets/inc/config.php';
?>

<!DOCTYPE html>
<html lang="ja">

  <head>
    <?php include_once $_SERVER['DOCUMENT_ROOT'].'/'.DIRNAME.'/assets/inc/html-head.php'; ?>
  </head>

  <body>
    <?php include_once $_SERVER['DOCUMENT_ROOT'].'/'.DIRNAME.'/assets/inc/load_script.php'; ?>
    <?php include_once $_SERVER['DOCUMENT_ROOT'].'/'.DIRNAME.'/assets/inc/blocks/svgs.php'; ?>
    <?php include_once $_SERVER['DOCUMENT_ROOT'].'/'.DIRNAME.'/assets/inc/tagmanager.php'; ?>

    <a name="top" id="top"></a>

    <?php include_once $_SERVER['DOCUMENT_ROOT'].'/'.DIRNAME.'/assets/inc/blocks/header.php'; ?>

    <main class="contents">
      <div class="contents__inner">

      <?php include_once $_SERVER['DOCUMENT_ROOT'].'/'.DIRNAME.'/assets/inc/blocks/shop-list.php'; ?>

      </div>
    </main>

    <?php include_once $_SERVER['DOCUMENT_ROOT'].'/'.DIRNAME.'/assets/inc/blocks/footer.php'; ?>

    <?php include_once $_SERVER['DOCUMENT_ROOT'].'/'.DIRNAME.'/assets/inc/scripts.php'; ?>
  </body>

</html>
