<?php

include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/global-config.php';
//下記のパス「demo」を変更
include_once $_SERVER['DOCUMENT_ROOT'].'/page/_new_renew_type01/assets/inc/config.php';

$nowURL = $_SERVER['HTTP_HOST'];
$pageURL = STORE_NAME.'.parco.jp';
$noCacheURL = STORE_NAME.'-parco.sc-concierge.jp';
$devURL = 'dev-'.STORE_NAME.'-parco.sc-concierge.jp';

//ドメインの判定
if ($nowURL === $pageURL || $nowURL === $noCacheURL || $devURL === $pageURL) {
  $productionFlag = true; //本番とキャッシュなしサーバとdev
} else {
  $productionFlag = false; //上記以外
}

if ($productionFlag === true) {
  //basic include
  include_once $_SERVER['DOCUMENT_ROOT'] . '/inc/page_include.php';
}

//ページ用の変数
$pege_title = 'NEW&RENEWテンプレート　TYPE02';
$pege_description = 'ディスクリプションが入ります';
$pege_keywords = 'キーワード';
$page_shareurl = 'https://'.STORE_NAME.'.parco.jp/page/_new_renew_type02/'; //必ずディレクトリ名を変更する

?>
<!DOCTYPE html>
<html lang="ja">
<head>
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/page/assets/inc/meta.php';?>

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/tagmanager1.php';?>

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/'.DIRNAME.'/assets/inc/css.php';?>
</head>

<body class="<?php echo STORE_NAME; ?> page-tag" id="top">
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/tagmanager2.php';?>

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/page/assets/inc/svgs.php';?>

<div class="wrapper">

<?php
/**
 * Header
 */
?>
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/'.DIRNAME.'/assets/inc/header.php'; ?>


<?php
/**
 * Main contents
 */
?>
<main class="main-contents">

<nav class="localnav">
  <ul class="localnav-list">
    <li class="localnav-item"><a href="#ctg01" class="scroll">カテゴリー1</a></li>
    <li class="localnav-item"><a href="#ctg02" class="scroll">カテゴリー2</a></li>
    <li class="localnav-item"><a href="#ctg03" class="scroll">カテゴリー3</a></li>
  </ul>
<!-- /.localnav --></nav>

<div class="newrenew-block" id="shoplist" v-cloak>

  <div class="list-block" id="ctg01">
    <h2 class="ttl">カテゴリー1</h2>
    <ul class="item-list js-tabarea"  v-cloak>
      <li class="item" v-for="(list01, index) in list01" v-bind:key="index">

        <?php /* 表で表示 */ ?>
        <div class="front-cont">
          <div class="img-wrap">
            <div class="badge-l">
              <p class="date"><span v-if="list01.date != ''">{{list01.date}}</span><br><span v-if="list01.weeks != ''">{{list01.weeks}}</span></p>
              <p class="new-renew">{{list01.new_renew}}</p>
            </div>
            <div class="badge-r" v-if="list01.badge != ''"><p class="txt">{{list01.badge}}</p></div>
            <div class="img"><img :src="list01.image01" :alt="list01.name"></div>
          <!-- /.img-wrap --></div>
          <div class="detail">
            <div class="logo"><img :src="list01.logo" :alt="list01.name"></div>
            <p class="shop">{{list01.name}}</p>
            <p class="floor">{{list01.yakata}} {{list01.floor}}</p>
            <p class="ctg">{{list01.category_name}}</p>

              <div class="more"><button class="js-modalopen" :data-modal="'modal01-'+index">MORE</button></div>
          <!-- /.detail --></div>
        <!-- /.front-cont --></div>

        <?php /* モーダル  */ ?>
        <div :class="'modal-block js-modal newrenew-modal modal01-' + index">
          <div class="modal-bg js-modalclose"></div>
          <div class="modal-wrap">
            <button class="modal-close js-modalclose"></button>

            <div class="modal-cont">
              <div class="img-wrap">
                <div class="img-slider js-imgslider">
                  <div class="img"><img :src="list01.image01" :alt="list01.name"></div>
                  <div class="img" v-if="list01.image02 != ''"><img :src="list01.image02" :alt="list01.name"></div>
                  <div class="img" v-if="list01.image03 != ''"><img :src="list01.image03" :alt="list01.name"></div>
                  <div class="img" v-if="list01.image04 != ''"><img :src="list01.image04" :alt="list01.name"></div>
                  <div class="img" v-if="list01.image05 != ''"><img :src="list01.image05" :alt="list01.name"></div>
                </div>
              <!-- /.img-wrap --></div>
              <div class="detail">
                <p class="date"><span v-if="list01.date != ''">{{list01.date}}</span><span v-if="list01.weeks != ''">{{list01.weeks}}</span></p>
                <p class="new-renew">{{list01.new_renew}}</p>
                <p class="first" v-if="list01.badge != ''">{{list01.badge}}</p>
                <div class="logo"><img :src="list01.logo" :alt="list01.name"></div>
                <p class="shop">{{list01.name}}</p>
                <p class="floor">{{list01.yakata}} {{list01.floor}}</p>
                <p class="ctg">{{list01.category_name}}</p>
                <p class="introduction" v-html="brTxt(list01.text)"></p>
                <div class="open-event" v-if="list01.opening != ''">
                  <p class="ttl">オープニング企画</p>
                  <p class="txt" v-html="brTxt(list01.opening)">企画内容</p>
                </div>
                <ul class="link-list">
                  <li v-if="list01.link_page != ''" class="link-item home"><a :href="list01.link_page" target="_blank"><img src="/<?php echo DIRNAME; ?>/assets/images/ico_home.png" alt="HOME"></a></li>
                  <li v-if="list01.link_recruit != ''" class="link-item recruit"><a :href="list01.link_recruit" target="_blank"><img src="/<?php echo DIRNAME; ?>/assets/images/ico_recruit.png" alt="STAFF RECRUIT"></a></li>
                  <li v-if="list01.link_twitter != ''" class="link-item tw"><a :href="list01.link_twitter" target="_blank"><img src="/<?php echo DIRNAME; ?>/assets/images/ico_tw.png" alt="Twitter"></a></li>
                  <li v-if="list01.link_facebook != ''" class="link-item fb"><a :href="list01.link_facebook" target="_blank"><img src="/<?php echo DIRNAME; ?>/assets/images/ico_fb.png" alt="Facebook"></a></li>
                  <li v-if="list01.link_instagram != ''" class="link-item ig"><a :href="list01.link_instagram" target="_blank"><img src="/<?php echo DIRNAME; ?>/assets/images/ico_ig.png" alt="Instagram"></a></li>
                <!-- /.link-list --></ul>
              <!-- /.detail --></div>
            </div>

            <div class="modal-arrow prev" v-if=modalPrev(index)><button class="js-modalopen" :data-modal="'modal01-'+ (index - 1)">前のアイテム</button></div>
            <div class="modal-arrow next" v-if=modalNext(index,'list01')><button class="js-modalopen" :data-modal="'modal01-'+ (index + 1)">次のアイテム</button></div>
          <!-- /.modal-block --></div>
        </div>
      </li>
    </ul>
  </div><!-- /.list-block -->

  <div class="list-block" id="ctg02">
    <h2 class="ttl">カテゴリー2</h2>
    <ul class="item-list js-tabarea"  v-cloak>
      <li class="item" v-for="(list02, index) in list02" v-bind:key="index">

        <?php /* 表で表示 */ ?>
        <div class="front-cont">
          <div class="img-wrap">
            <div class="badge-l">
              <p class="date"><span v-if="list02.date != ''">{{list02.date}}</span><br><span v-if="list02.weeks != ''">{{list02.weeks}}</span></p>
              <p class="new-renew">{{list02.new_renew}}</p>
            </div>
            <div class="badge-r" v-if="list02.badge != ''"><p class="txt">{{list02.badge}}</p></div>
            <div class="img"><img :src="list02.image01" :alt="list02.name"></div>
          <!-- /.img-wrap --></div>
          <div class="detail">
            <div class="logo"><img :src="list02.logo" :alt="list02.name"></div>
            <p class="shop">{{list02.name}}</p>
            <p class="floor">{{list02.yakata}} {{list02.floor}}</p>
            <p class="ctg">{{list02.category_name}}</p>

              <div class="more"><button class="js-modalopen" :data-modal="'modal02-'+index">MORE</button></div>
          <!-- /.detail --></div>
        <!-- /.front-cont --></div>

        <?php /* モーダル  */ ?>
        <div :class="'modal-block js-modal newrenew-modal modal02-' + index">
          <div class="modal-bg js-modalclose"></div>
          <div class="modal-wrap">
            <button class="modal-close js-modalclose"></button>

            <div class="modal-cont">
              <div class="img-wrap">
                <div class="img-slider js-imgslider">
                  <div class="img"><img :src="list02.image01" :alt="list02.name"></div>
                  <div class="img" v-if="list02.image02 != ''"><img :src="list02.image02" :alt="list02.name"></div>
                  <div class="img" v-if="list02.image03 != ''"><img :src="list02.image03" :alt="list02.name"></div>
                  <div class="img" v-if="list02.image04 != ''"><img :src="list02.image04" :alt="list02.name"></div>
                  <div class="img" v-if="list02.image05 != ''"><img :src="list02.image05" :alt="list02.name"></div>
                </div>
              <!-- /.img-wrap --></div>
              <div class="detail">
                <p class="date"><span v-if="list02.date != ''">{{list02.date}}</span><span v-if="list02.weeks != ''">{{list02.weeks}}</span></p>
                <p class="new-renew">{{list02.new_renew}}</p>
                <p class="first" v-if="list02.badge != ''">{{list02.badge}}</p>
                <div class="logo"><img :src="list02.logo" :alt="list02.name"></div>
                <p class="shop">{{list02.name}}</p>
                <p class="floor">{{list02.yakata}} {{list02.floor}}</p>
                <p class="ctg">{{list02.category_name}}</p>
                <p class="introduction" v-html="brTxt(list02.text)"></p>
                <div class="open-event" v-if="list02.opening != ''">
                  <p class="ttl">オープニング企画</p>
                  <p class="txt" v-html="brTxt(list02.opening)">企画内容</p>
                </div>
                <ul class="link-list">
                  <li v-if="list02.link_page != ''" class="link-item home"><a :href="list02.link_page" target="_blank"><img src="/<?php echo DIRNAME; ?>/assets/images/ico_home.png" alt="HOME"></a></li>
                  <li v-if="list02.link_recruit != ''" class="link-item recruit"><a :href="list02.link_recruit" target="_blank"><img src="/<?php echo DIRNAME; ?>/assets/images/ico_recruit.png" alt="STAFF RECRUIT"></a></li>
                  <li v-if="list02.link_twitter != ''" class="link-item tw"><a :href="list02.link_twitter" target="_blank"><img src="/<?php echo DIRNAME; ?>/assets/images/ico_tw.png" alt="Twitter"></a></li>
                  <li v-if="list02.link_facebook != ''" class="link-item fb"><a :href="list02.link_facebook" target="_blank"><img src="/<?php echo DIRNAME; ?>/assets/images/ico_fb.png" alt="Facebook"></a></li>
                  <li v-if="list02.link_instagram != ''" class="link-item ig"><a :href="list02.link_instagram" target="_blank"><img src="/<?php echo DIRNAME; ?>/assets/images/ico_ig.png" alt="Instagram"></a></li>
                <!-- /.link-list --></ul>
              <!-- /.detail --></div>
            </div>

            <div class="modal-arrow prev" v-if=modalPrev(index)><button class="js-modalopen" :data-modal="'modal02-'+ (index - 1)">前のアイテム</button></div>
            <div class="modal-arrow next" v-if=modalNext(index,'list02')><button class="js-modalopen" :data-modal="'modal02-'+ (index + 1)">次のアイテム</button></div>
          <!-- /.modal-block --></div>
        </div>
      </li>
    </ul>
  </div><!-- /.list-block -->

  <div class="list-block" id="ctg03">
    <h2 class="ttl">カテゴリー3</h2>
    <ul class="item-list js-tabarea"  v-cloak>
      <li class="item" v-for="(list03, index) in list03" v-bind:key="index">

        <?php /* 表で表示 */ ?>
        <div class="front-cont">
          <div class="img-wrap">
            <div class="badge-l">
              <p class="date"><span v-if="list03.date != ''">{{list03.date}}</span><br><span v-if="list03.weeks != ''">{{list03.weeks}}</span></p>
              <p class="new-renew">{{list03.new_renew}}</p>
            </div>
            <div class="badge-r" v-if="list03.badge != ''"><p class="txt">{{list03.badge}}</p></div>
            <div class="img"><img :src="list03.image01" :alt="list03.name"></div>
          <!-- /.img-wrap --></div>
          <div class="detail">
            <div class="logo"><img :src="list03.logo" :alt="list03.name"></div>
            <p class="shop">{{list03.name}}</p>
            <p class="floor">{{list03.yakata}} {{list03.floor}}</p>
            <p class="ctg">{{list03.category_name}}</p>

              <div class="more"><button class="js-modalopen" :data-modal="'modal03-'+index">MORE</button></div>
          <!-- /.detail --></div>
        <!-- /.front-cont --></div>

        <?php /* モーダル  */ ?>
        <div :class="'modal-block js-modal newrenew-modal modal03-' + index">
          <div class="modal-bg js-modalclose"></div>
          <div class="modal-wrap">
            <button class="modal-close js-modalclose"></button>

            <div class="modal-cont">
              <div class="img-wrap">
                <div class="img-slider js-imgslider">
                  <div class="img"><img :src="list03.image01" :alt="list03.name"></div>
                  <div class="img" v-if="list03.image02 != ''"><img :src="list03.image02" :alt="list03.name"></div>
                  <div class="img" v-if="list03.image03 != ''"><img :src="list03.image03" :alt="list03.name"></div>
                  <div class="img" v-if="list03.image04 != ''"><img :src="list03.image04" :alt="list03.name"></div>
                  <div class="img" v-if="list03.image05 != ''"><img :src="list03.image05" :alt="list03.name"></div>
                </div>
              <!-- /.img-wrap --></div>
              <div class="detail">
                <p class="date"><span v-if="list03.date != ''">{{list03.date}}</span><span v-if="list03.weeks != ''">{{list03.weeks}}</span></p>
                <p class="new-renew">{{list03.new_renew}}</p>
                <p class="first" v-if="list03.badge != ''">{{list03.badge}}</p>
                <div class="logo"><img :src="list03.logo" :alt="list03.name"></div>
                <p class="shop">{{list03.name}}</p>
                <p class="floor">{{list03.yakata}} {{list03.floor}}</p>
                <p class="ctg">{{list03.category_name}}</p>
                <p class="introduction" v-html="brTxt(list03.text)"></p>
                <div class="open-event" v-if="list03.opening != ''">
                  <p class="ttl">オープニング企画</p>
                  <p class="txt" v-html="brTxt(list03.opening)">企画内容</p>
                </div>
                <ul class="link-list">
                  <li v-if="list03.link_page != ''" class="link-item home"><a :href="list03.link_page" target="_blank"><img src="/<?php echo DIRNAME; ?>/assets/images/ico_home.png" alt="HOME"></a></li>
                  <li v-if="list03.link_recruit != ''" class="link-item recruit"><a :href="list03.link_recruit" target="_blank"><img src="/<?php echo DIRNAME; ?>/assets/images/ico_recruit.png" alt="STAFF RECRUIT"></a></li>
                  <li v-if="list03.link_twitter != ''" class="link-item tw"><a :href="list03.link_twitter" target="_blank"><img src="/<?php echo DIRNAME; ?>/assets/images/ico_tw.png" alt="Twitter"></a></li>
                  <li v-if="list03.link_facebook != ''" class="link-item fb"><a :href="list03.link_facebook" target="_blank"><img src="/<?php echo DIRNAME; ?>/assets/images/ico_fb.png" alt="Facebook"></a></li>
                  <li v-if="list03.link_instagram != ''" class="link-item ig"><a :href="list03.link_instagram" target="_blank"><img src="/<?php echo DIRNAME; ?>/assets/images/ico_ig.png" alt="Instagram"></a></li>
                <!-- /.link-list --></ul>
              <!-- /.detail --></div>
            </div>

            <div class="modal-arrow prev" v-if=modalPrev(index)><button class="js-modalopen" :data-modal="'modal03-'+ (index - 1)">前のアイテム</button></div>
            <div class="modal-arrow next" v-if=modalNext(index,'list03')><button class="js-modalopen" :data-modal="'modal03-'+ (index + 1)">次のアイテム</button></div>
          <!-- /.modal-block --></div>
        </div>
      </li>
    </ul>
  </div><!-- /.list-block -->

</div>

</main>





<?php
/**
 * Footer
 */
?>
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/page/assets/inc/footer.php'; ?>

</div>

<!-- Javascript -->
<script>
  const list = '/<?php echo DIRNAME; ?>/assets/data/shoplist.json';
</script>
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/'.DIRNAME.'/assets/inc/js.php';?>

<script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
<script src="https://www.promisejs.org/polyfills/promise-7.0.4.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="/<?php echo DIRNAME; ?>/assets/js/lib/slick.min.js"></script>
<script src="/<?php echo DIRNAME; ?>/assets/js/newrenew.js"></script>
<!-- /Javascript -->
</body>
</html>
