<?php

include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/global-config.php';
//下記のパス「demo」を変更
include_once $_SERVER['DOCUMENT_ROOT'].'/page/_new_renew_type01/assets/inc/config.php';

$nowURL = $_SERVER['HTTP_HOST'];
$pageURL = STORE_NAME.'.parco.jp';
$noCacheURL = STORE_NAME.'-parco.sc-concierge.jp';
$devURL = 'dev-'.STORE_NAME.'-parco.sc-concierge.jp';

//ドメインの判定
if ($nowURL === $pageURL || $nowURL === $noCacheURL || $devURL === $pageURL) {
  $productionFlag = true; //本番とキャッシュなしサーバとdev
} else {
  $productionFlag = false; //上記以外
}

if ($productionFlag === true) {
  //basic include
  include_once $_SERVER['DOCUMENT_ROOT'] . '/inc/page_include.php';
}

//ページ用の変数
$pege_title = 'NEW&RENEWテンプレート　TYPE03';
$pege_description = 'ディスクリプションが入ります';
$pege_keywords = 'キーワード';
$page_shareurl = 'https://'.STORE_NAME.'.parco.jp/page/_new_renew_type03/'; //必ずディレクトリ名を変更する

?>
<!DOCTYPE html>
<html lang="ja">
<head>
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/page/assets/inc/meta.php';?>

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/tagmanager1.php';?>

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/'.DIRNAME.'/assets/inc/css.php';?>
</head>

<body class="<?php echo STORE_NAME; ?> page-tag" id="top">
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/tagmanager2.php';?>

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/page/assets/inc/svgs.php';?>

<div class="wrapper">

<?php
/**
 * Header
 */
?>
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/'.DIRNAME.'/assets/inc/header.php'; ?>


<?php
/**
 * Main contents
 */
?>
<main class="main-contents">

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/'.DIRNAME.'/assets/inc/localnav.php'; ?>

<div class="newrenew-block" id="shoplist" v-cloak>

  <div class="list-block" id="ctg02">
    <h2 class="ttl">カテゴリー2</h2>
    <ul class="item-list" v-cloak>
      <li class="item" v-for="(list02, index) in list02" v-bind:key="index">

        <?php /* 表で表示 */ ?>
        <div class="front-cont">
          <div class="img-wrap">
            <div class="badge-l">
              <p class="date"><template v-if="list02.date != ''">{{list02.date}}</template><br><template v-if="list02.weeks != ''">{{list02.weeks}}</template></p>
              <p class="new-renew">{{list02.new_renew}}</p>
            </div>
            <div class="badge-r" v-if="list02.badge != ''"><p class="txt">{{list02.badge}}</p></div>
            <div class="img"><img :src="list02.image01" :alt="list02.name"></div>
          <!-- /.img-wrap --></div>
          <div class="detail">
            <div class="logo"><img :src="list02.logo" :alt="list02.name"></div>
            <p class="shop">{{list02.name}}</p>
            <p class="floor">{{list02.yakata}} {{list02.floor}}</p>
            <p class="ctg">{{list02.category_name}}</p>

              <div class="more"><button class="js-modalopen" :data-modal="'modal-'+index">MORE</button></div>
          <!-- /.detail --></div>
        <!-- /.front-cont --></div>

        <?php /* モーダル  */ ?>
        <div :class="'modal-block js-modal newrenew-modal modal-' + index">
          <div class="modal-bg js-modalclose"></div>
          <div class="modal-wrap">
            <button class="modal-close js-modalclose"></button>

            <div class="modal-cont">
              <div class="img-wrap">
                <div class="img-slider js-imgslider">
                  <div class="img"><img :src="list02.image01" :alt="list02.name"></div>
                  <div class="img" v-if="list02.image02 != ''"><img :src="list02.image02" :alt="list02.name"></div>
                  <div class="img" v-if="list02.image03 != ''"><img :src="list02.image03" :alt="list02.name"></div>
                  <div class="img" v-if="list02.image04 != ''"><img :src="list02.image04" :alt="list02.name"></div>
                  <div class="img" v-if="list02.image05 != ''"><img :src="list02.image05" :alt="list02.name"></div>
                </div>
              <!-- /.img-wrap --></div>
              <div class="detail">
                <p class="date"><template v-if="list02.date != ''">{{list02.date}}</template><template v-if="list02.weeks != ''">{{list02.weeks}}</template></p>
                <p class="new-renew">{{list02.new_renew}}</p>
                <p class="first" v-if="list02.badge != ''">{{list02.badge}}</p>
                <div class="logo"><img :src="list02.logo" :alt="list02.name"></div>
                <p class="shop">{{list02.name}}</p>
                <p class="floor">{{list02.yakata}} {{list02.floor}}</p>
                <p class="ctg">{{list02.category_name}}</p>
                <p class="introduction" v-html="brTxt(list02.text)"></p>
                <div class="open-event" v-if="list02.opening != ''">
                  <p class="ttl">オープニング企画</p>
                  <p class="txt" v-html="brTxt(list02.opening)">企画内容</p>
                </div>
                <ul class="link-list">
                  <li v-if="list02.link_page != ''" class="link-item home"><a :href="list02.link_page" target="_blank"><img src="/<?php echo DIRNAME; ?>/assets/images/ico_home.png" alt="HOME"></a></li>
                  <li v-if="list02.link_recruit != ''" class="link-item recruit"><a :href="list02.link_recruit" target="_blank"><img src="/<?php echo DIRNAME; ?>/assets/images/ico_recruit.png" alt="STAFF RECRUIT"></a></li>
                  <li v-if="list02.link_twitter != ''" class="link-item tw"><a :href="list02.link_twitter" target="_blank"><img src="/<?php echo DIRNAME; ?>/assets/images/ico_tw.png" alt="Twitter"></a></li>
                  <li v-if="list02.link_facebook != ''" class="link-item fb"><a :href="list02.link_facebook" target="_blank"><img src="/<?php echo DIRNAME; ?>/assets/images/ico_fb.png" alt="Facebook"></a></li>
                  <li v-if="list02.link_instagram != ''" class="link-item ig"><a :href="list02.link_instagram" target="_blank"><img src="/<?php echo DIRNAME; ?>/assets/images/ico_ig.png" alt="Instagram"></a></li>
                <!-- /.link-list --></ul>
              <!-- /.detail --></div>
            </div>

            <div class="modal-arrow prev" v-if=modalPrev(index)><button class="js-modalopen" :data-modal="'modal-'+ (index - 1)">前のアイテム</button></div>
            <div class="modal-arrow next" v-if=modalNext(index,'list02')><button class="js-modalopen" :data-modal="'modal-'+ (index + 1)">次のアイテム</button></div>
          <!-- /.modal-wrap --></div>
        <!-- /.modal-block --></div>
      </li>
    </ul>
  </div><!-- /.list-block -->

</div>

</main>





<?php
/**
 * Footer
 */
?>
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/page/assets/inc/footer.php'; ?>

</div>

<!-- Javascript -->
<script>
  const list = '/<?php echo DIRNAME; ?>/assets/data/shoplist.json';
</script>
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/'.DIRNAME.'/assets/inc/js.php';?>

<script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
<script src="https://www.promisejs.org/polyfills/promise-7.0.4.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="/<?php echo DIRNAME; ?>/assets/js/lib/slick.min.js"></script>
<script src="/<?php echo DIRNAME; ?>/assets/js/newrenew.js"></script>
<!-- /Javascript -->
</body>
</html>
