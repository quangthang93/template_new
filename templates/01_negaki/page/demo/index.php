<?php

include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/global-config.php';
//下記のパス「demo」を変更
include_once $_SERVER['DOCUMENT_ROOT'].'/page/demo/assets/inc/config.php';

$nowURL = $_SERVER['HTTP_HOST'];
$pageURL = STORE_NAME.'.parco.jp';
$noCacheURL = STORE_NAME.'-parco.sc-concierge.jp';
$devURL = 'dev-'.STORE_NAME.'-parco.sc-concierge.jp';

//ドメインの判定
if ($nowURL === $pageURL || $nowURL === $noCacheURL || $devURL === $pageURL) {
  $productionFlag = true; //本番とキャッシュなしサーバとdev
} else {
  $productionFlag = false; //上記以外
}

if ($productionFlag === true) {
  //basic include
  include_once $_SERVER['DOCUMENT_ROOT'] . '/inc/page_include.php';
}

//ページ用の変数
$pege_title = 'タイトル';
$pege_description = 'ディスクリプションが入ります';
$pege_keywords = 'キーワード';
$page_shareurl = 'https://'.STORE_NAME.'.parco.jp/page/demo/'; //必ずディレクトリ名を変更する

?>
<!DOCTYPE html>
<html lang="ja">
<head>
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/page/assets/inc/meta.php';?>

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/tagmanager1.php';?>

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/'.DIRNAME.'/assets/inc/css.php';?>
</head>

<body class="<?php echo STORE_NAME; ?> page-tag" id="top">
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/tagmanager2.php';?>

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/page/assets/inc/svgs.php';?>

<div class="wrapper">

<?php
/**
 * Header
 */
?>
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/'.DIRNAME.'/assets/inc/header.php'; ?>


<?php
/**
 * Main contents
 */
?>
<main class="main-contents">

<?php /* ==========================================================
ヘッダー下バナー、youtubeなど
============================================================== */ ?>
<?php /* バナー */ ?>
<div class="top-bnr-block">
  <div class="bnr"><a href="#" target="_blank">
    <picture>
      <?php // SPバナー ?>
      <source media="(max-width: 768px)" srcset="/<?php echo DIRNAME; ?>/assets/images/bnr1_sp.png">
      <?php // PCバナー ?>
      <img src="/<?php echo DIRNAME; ?>/assets/images/bnr1_pc.png" alt="">
    </picture>
  </a></div>
  <div class="bnr"><a href="#" target="_blank">
    <picture>
      <?php // SPバナー  ?>
      <source media="(max-width: 768px)" srcset="/<?php echo DIRNAME; ?>/assets/images/bnr1_sp.png">
      <?php // PCバナー  ?>
      <img src="/<?php echo DIRNAME; ?>/assets/images/bnr1_pc.png" alt="">
    </picture>
  </a></div>
<!-- /.top-bnr-block --></div>

<?php /* YouTube */ ?>
<div class="youtube-block">
  <div class="youtube-wrap">
    <iframe src="https://www.youtube.com/embed/sll7aYhwBUo?rel=0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  </div>
<!-- /.youtube-block --></div>


<?php /* ==========================================================
Local navigation
============================================================== */ ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/'.DIRNAME.'/assets/inc/localnav.php'; ?>

<?php /* ==========================================================
タグ集約、ownlyなど
============================================================== */ ?>
<div class="contents-wrap">

  <?php /* PARCO NEWS */ ?>
  <div class="contents-block contents-blog newsevent">
    <h2 class="contents-ttl">PARCO NEWS</h2>
    <?php
    if ($productionFlag === true) {

      // 本番用
      $disp_data = array(); //初期化
      $tagname = "福袋"; //対象するタグの文字列をセット
      $data_target = "pnews";
        //pnews:パルコニュースのみ
        //shopnews:ショップニュースのみ
        //all:すべて
        //popupnews: POPUPSHOP（名古屋のみ使用可）
        //entnews: エンタテインメントニュース（名古屋のみ使用可）
      echo "<!-- タグ 「".$tagname."」-->\n"; //確認用
      echo "<!-- カテゴリ 「".$data_target."」-->\n\n"; //確認用
      include $_SERVER['DOCUMENT_ROOT'] . '/inc/tagsum_include.php';
      include $_SERVER['DOCUMENT_ROOT'].'/page/assets/inc/item-list.php';

    } else {

      // 静的デモ レイアウト調整用
      include $_SERVER['DOCUMENT_ROOT'].'/page/assets/inc/item-list-demo.php';

    }
    ?>
  <!-- /.contents-block --></div>



  <?php /* SHOP NEWS */ ?>
  <div class="contents-block contents-blog shopblog">
    <h2 class="contents-ttl">SHOP NEWS</h2>
    <?php
    if ($productionFlag === true) {

      // 本番用
      $disp_data = array(); //初期化
      $tagname = "メンズ"; //対象するタグの文字列をセット
      $data_target = "shopnews";
        //pnews:パルコニュースのみ
        //shopnews:ショップニュースのみ
        //all:すべて
        //popupnews: POPUPSHOP（名古屋のみ使用可）
        //entnews: エンタテインメントニュース（名古屋のみ使用可）
      echo "<!-- タグ 「".$tagname."」-->\n"; //確認用
      echo "<!-- カテゴリ 「".$data_target."」-->\n\n"; //確認用
      include $_SERVER['DOCUMENT_ROOT'] . '/inc/tagsum_include.php';
      include $_SERVER['DOCUMENT_ROOT'].'/page/assets/inc/item-list.php';

    } else {

      // 静的デモ レイアウト調整用
      include $_SERVER['DOCUMENT_ROOT'].'/page/assets/inc/item-list-demo.php';
    }

    ?>
  <!-- /.contents-block --></div>



  <?php /* OWNLY */ ?>
  <div class="contents-block contents-ownly">
    <h2 class="contents-ttl">OWNLY</h2>
    <div class="ownly-wrap"><script src="//parco.ownly.jp/story/if/4247?itype=3&amp;itpl=16&amp;icnt=8"></script></div>
  <!-- /.contents-block --></div>
<!-- /.contents-wrap --></div>


<?php /* ==========================================================
フッター上バナーなど
============================================================== */ ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/page/assets/inc/foot-banner.php'; ?>

</main>


<?php
/**
 * Footer
 */
?>
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/page/assets/inc/footer.php'; ?>

</div>

<!-- Javascript -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/'.DIRNAME.'/assets/inc/js.php';?>
<!-- /Javascript -->
</body>
</html>
