#概要

ここでは主に **値書き** と **タグ集約** と呼ばれるページについて説明します。
対象は2018年にリニューアルが終わっている店舗配下に設置するページです。

**値書き**
各テナントのセールのOFF率をリストにしたページ

**タグ集約**
各テナントやPARCOの各館が投稿したブログ記事をタグによって収集したページ

**旧テンプレートとの違い**

- レスポンシブ化された
- 各店舗とも/page/配下に設置
- ブログ集約がタグ集約に変更
- テンプレート内にダミーデータ（ただの静的なhtml）を用意することでローカルでも作業可能になった


##値書き

仙台店を例に説明します。
仙台店は本館とPARCO2館と2館です。

##値書きのURL

本館
https://sendai.parco.jp/page/sale/?slcd=000015&pv=on
上記のURLは下記でも閲覧可能
https://sendai.parco.jp/page/sale/?slcd=000015&floor_type=type1&pv=on

PARCO2
https://sendai.parco.jp/page/sale/?slcd=000015&floor_type=type2&pv=on

非公開時でも下記のように「 **&pv=on** 」をつけることで閲覧が可能になります。

## ファイル構成

###値書きページのファイル

/page/sale/000015_type1.php ※本館
/page/sale/000015_type2.php ※PARCO2

旧テンプレートと違い、館の数だけファイルが必要です。
仙台店は2館なので上記のように2ファイルあります。

またファイルの命名ルールについては「 **＜値書きID＞_type＜館番号＞** 」となります。
館番号に関しては全店舗共通でtype1〜type4（名古屋の4館が最大）です。

### 静的ファイル

/page/XXXX/assets/

上記に全て格納してください。
タグ集約とセットの企画の際は別途作成する必要はありませんが、
値書きのみの企画の際は上記を使用してください。


## フロー
1. デモページのテンプレートファイル（indexXXXX.phtml）とstatic配下のファイルを複製する
2. static/XXXXX/inc/config.phpの設定を案件に合わせて修正する
3. テンプレートファイル先頭付近のconfig.phpのパスを変更
4. サーバにアップロード
5. デザインの調整など（コーディング）を行う

### 1. デモページのテンプレートファイル（indexXXXX.phtml）とstatic配下のファイルを複製する

デモページのファイルを1式サーバから落としてください。
（今後、Gitで管理したものを使用していただく想定です。）

http://sapporo.parco.jp/page/check/?view=page2403&pv=on
http://sapporo.parco.jp/spage/check/?view=page2403&pv=on

### 2. static/XXXXX/inc/config.phpの設定を案件に合わせて修正する

css、js、メインビジュアル、snsボタンの設定URLなど企画ごとに変わる部分に関してはconfig.phpに定数としてまとめて記述します。
なのではじめにこの変更を行うことで作業がしやすくなります。
各項目についてはコメントを参照してください。

尚、config.phpは **PC、スマホ共通で使用** します。
またPC側のディレクトリにのみ置きます。

```
<?php
/* 企画ごとの設定 */
define('STORE_NAME', 'sapporo'); //PARCO 店名 英語 （ドメインに使用している文字列と共通）
define('STORE_NAME_JA', '札幌PARCO'); //PARCO 店名 日本語
define('DIRNAME', 'test-check'); //static配下に置くディレクトリ名
define('TITLE', '〈PARCOカード〉ご利用でおトクな7日間｜札幌PARCO'); //titleなどに使用
define('DESCRIPTION', '札幌パルコ 〈PARCOカード〉5%OFF「〈PARCOカード〉ご利用でおトクな7日間」参加ショップ情報や、その他お得なイベント、商品など、札幌PARCOの情報を紹介しています。'); //metaなどに使用
define('KEYWORDS', '札幌パルコ,PARCO,SALE,セール,EVENT,イベント,〈PARCOカード〉'); //metaなどに使用
$sns_url = 'http://sapporo.parco.jp/page/check/?view=page2403'; //snsボタン用
$sns_url_sp = 'http://sapporo.parco.jp/spage/check/?view=page2403'; //snsボタン用

/* 値書きの設定 */
define('CHECK_NUM', '2403'); //値書きのページID

/* ブログ集約の設定 */
define('POST_SDATE1', '20160909000000'); //ブログ記事の開始日
define('POST_EDATE1', '20160930210000'); //ブログ記事の終了日
//define('POSTS_SDATE2', '20160401000000'); //ブログ記事の開始日
//define('POSTS_EDATE2', '20170401000000'); //ブログ記事の終了日
define('CATEGORY_ID1', '170'); //ブログ記事のカテゴリ（上段）
define('CATEGORY_ID2', '32'); //ブログ記事のカテゴリ（下段）
?>
```

### 3. テンプレートファイル先頭付近のconfig.phpのパスを変更

PC、スマホ共にルートパスで記述していますのでディレクトリ名だけ変更してください。
（下記の「test-check」という文字列を変更）

```
include_once($_SERVER['DOCUMENT_ROOT']. '/static/test-check/inc/config.php');
```

### 4. サーバにアップロード

staticに置く企画ごとのディレクトリとテンプレートファイルをそれぞれUPします。
ソースを確認し企画用のディレクトリなど正しいかご確認ください。
間違っている場合はconfig.php内の記述か、テンプレートファイルのインクルードのconfig.phpのパスが間違っていると可能性がありますので見直してください。
問題なく表示されていればこのままデザインの変更作業に入ります。

### 5. デザインの調整など（コーディング）を行う

主にPCで話を進めます。
スマホも概ね同じと思ってください。
変更がある部分は下記の通りです。

- 全体の背景
- メインビジュアル
- タブナビゲーション（ブログ集約などとセットの企画のときには必要）
- フロアの見出し画像
- フッターのボタンとコピーライト

逆にメインビジュアル上にある黒い帯部分に関しては大抵の企画で共通に入ります。
一切変更せずに入れてください。

#### フロアの見出し画像

デフォルトのフロアの見出し画像は各店舗とも/images/check/に画像が格納されています。
デザインの変更時には画像をstatic配下の規格用ディレクトリに格納の上でテンプレートファイル内のパスを変更してください。
パスの変更とは言え、変更時のディレクトリ名はconfig.phpで記述している定数を利用したソースに変更するだけです。
コメントを適宜外してください。

index2403.phtml 126行目

```
<?php /* フロアの表示 #デフォルト用 ?><?php */ ?>
<img class="contents-title__item01" src="/images/check/txt_fl_<?php print strtolower($value); ?>.png" alt="<?php print $value; ?>" />
<?php /* フロアの表示 #デフォルト用 ?>
<img class="contents-title__item01" src="/web/<?=DIRNAME?>/images/fl_<?php print strtolower($value); ?>.png" alt="<?php print $value; ?>" />
<?php */ ?>
<?php /* 館の表示 #デフォルト用 ?>
<img class="contents-title__item02" src="/images/check/txt_flt_<?php print $this->floorType; ?>.png" alt="<?php print $floorName; ?>" />
<?php */ ?>
<?php /* 館の表示 #カスタマイズ用 ?>
<img class="contents-title__item02" src="/web/<?=DIRNAME?>/images/txt_flt_<?php print $this->floorType; ?>.png" alt="<?php print $floorName; ?>" />
<?php */ ?>
```

![image][check-pc.jpg]
